import math
import ctypes
from colorama import init
init()
from colorama import Fore, Back, Style

class TextTools:
    @staticmethod
    def get_repeating_str(num, str=" "):
        repeating_str = ""
        for x in range(0, num):
            repeating_str += str
        return repeating_str

    @staticmethod
    def auto_format_bytes(bytes):
        gb_in_b = math.pow(1024, 3)
        mb_in_b = math.pow(1024, 2)
        kb_in_b = 1024

        if bytes > gb_in_b:
            # Use Gigabyte format
            value = "{0:.2f}".format(bytes / gb_in_b)
            symbol = "GB"
            return f"{value} {symbol}"

        elif bytes > mb_in_b:
            value = "{0:.2f}".format(bytes / mb_in_b)
            symbol = "MB"
            return f"{value} {symbol}"

        elif bytes > kb_in_b:
            value = "{0:.2f}".format(bytes / kb_in_b)
            symbol = "KB"
            return f"{value} {symbol}"

        else:
            value = bytes
            symbol = "B"
            return f"{value} {symbol}" 

    @staticmethod
    def color_text(str, fore_color, back_color = Back.BLACK):
        return f"{Style.RESET_ALL}{fore_color}{back_color}{str}{Style.RESET_ALL}"