from filetools import FileTools
from texttools import TextTools
from grid import Grid

import ctypes
from colorama import Fore

class DriveGrid:
    def __init__(self, path_file):
        self.path_file = path_file

    def get_grid(self):
        drives = FileTools.get_lines(self.path_file)

        columns = ["DRIVE", "TOTAL", "USED", "FREE"]
        value_lists = [[], [], [], []]

        for drive in drives:
            free = ctypes.c_ulonglong(0)
            total = ctypes.c_ulonglong(0)
            ctypes.windll.kernel32.GetDiskFreeSpaceExW(ctypes.c_wchar_p(drive + ":"), None, ctypes.byref(total), ctypes.pointer(free))

            total_bytes = total.value
            free_bytes = free.value
            used_bytes = total_bytes - free_bytes

            usage_ratio = used_bytes / total_bytes
            usage_perc_str = "{0:.2f}".format(usage_ratio * 100) + "%"

            usage_color = Fore.GREEN
            if usage_ratio > 0.70:
                usage_color = Fore.YELLOW
            elif usage_ratio > 0.90:
                usage_color = Fore.RED

            colored_usage_perc_str = TextTools.color_text(f"{usage_perc_str}", usage_color)

            total_str = TextTools.auto_format_bytes(total_bytes)
            free_str = TextTools.auto_format_bytes(free_bytes)
            used_str = TextTools.auto_format_bytes(used_bytes)

            value_lists[0].append({"value": drive, "true_length": len(drive)})
            value_lists[1].append({"value": total_str, "true_length": len(total_str)})
            value_lists[2].append({"value": f"{used_str} ({colored_usage_perc_str})", "true_length": len(used_str) + 3 + len(usage_perc_str)})
            value_lists[3].append({"value": free_str, "true_length": len(free_str)})

        grid = Grid(columns, value_lists)

        return grid.to_string()
