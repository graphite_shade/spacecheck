from filetools import FileTools
from texttools import TextTools
from grid import Grid


class PathGrid:
    def __init__(self, path_file):
        self.path_file = path_file
        
    def get_grid(self):
        paths = FileTools.get_lines(self.path_file)

        columns = ["PATHS", "SIZE"]
        value_lists = [[], []]

        for path in paths:
            size = FileTools.get_size(path, True)
            formatted_size_str = TextTools.auto_format_bytes(size)

            value_lists[0].append({"value": path, "true_length": len(path)})
            value_lists[1].append({"value": formatted_size_str, "true_length": len(formatted_size_str)})

        grid = Grid(columns, value_lists)

        return grid.to_string()
