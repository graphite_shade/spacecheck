from pathgrid import PathGrid
from drivegrid import DriveGrid

print("")

path_grid = PathGrid("paths.txt")
print(path_grid.get_grid())

print("")

drive_grid = DriveGrid("drives.txt")
print(drive_grid.get_grid())

