from texttools import TextTools

class Grid:
    def __init__(self, columns, value_lists):
        self.columns = columns
        self.value_lists = value_lists

    def to_string(self):
        # PRINT COLUMN STRING
        column_str = ""
        line_str = ""
        row_strings = []

        for i in range(0, len(self.value_lists[0])):
            row_strings.append("")



        for i in range(0, len(self.columns)):
            
            # GET COLUMN STRING

            col_values = self.value_lists[i]
            max_len = max(val["true_length"] for val in col_values)
            col_len = len(self.columns[i])

            column_str += self.columns[i]
            if max_len > col_len:
                column_str += TextTools.get_repeating_str(max_len - col_len, " ")

            if i < len(self.columns):
                column_str += "    "

            # GET ROWS
            for j in range(0, len(row_strings)):
                row_strings[j] += self.value_lists[i][j]["value"]

                if col_len > max_len:
                    row_strings[j] += TextTools.get_repeating_str(col_len - self.value_lists[i][j]["true_length"], " ")

                # Add padding
                elif max_len > self.value_lists[i][j]["true_length"]:
                    row_strings[j] += TextTools.get_repeating_str(max_len - self.value_lists[i][j]["true_length"], " ")


                if i < len(self.columns):
                    row_strings[j] += "    "

                


        # PRINT ROWS
        grid_str = ""
        grid_str += column_str
        grid_str += "\n"


        for i in range(0, len(row_strings)):
            grid_str += row_strings[i]
            grid_str += "\n"

        return grid_str

    def print(self):
        grid_str = self.to_string()
        print(grid_str)
