import os

class FileTools:
    @staticmethod
    def get_lines(file_name):
        txt_file = open(file_name, "r")

        lines = txt_file.readlines()

        if len(lines) == 0:
            raise Exception(f"No lines in file {file_name}")

        lines = [x.strip() for x in lines if not (x.isspace())]
        return lines

    @staticmethod
    def get_size(path = '.', print_details = True):
        cumulative_size = 0
        try:
            for de in os.scandir(path):
                if de.is_file():
                    cumulative_size = cumulative_size + de.stat().st_size
                if de.is_dir():
                    cumulative_size = cumulative_size + FileTools.get_size(de.path, False)

        except PermissionError as e:
            print (f"Encountered permission error {e.errno}: {e.strerror} for {path}")

        return cumulative_size